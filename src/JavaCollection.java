import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * @Author Reshma
 * @Date 6/26/2018
 * @Month 06
 **/
public class JavaCollection {
    public List getAddList() {
        return addList;
    }

    public void setAddList(List addList) {
        this.addList = addList;
    }

    public Set getAddSet() {
        return addSet;
    }

    public void setAddSet(Set addSet) {
        this.addSet = addSet;
    }

    public Map getAddMAp() {
        return addMAp;
    }

    public void setAddMAp(Map addMAp) {
        this.addMAp = addMAp;
    }

    public Properties getAddProp() {
        return addProp;
    }

    public void setAddProp(Properties addProp) {
        this.addProp = addProp;
    }

    List addList;
    Set addSet;
    Map addMAp;
    Properties addProp;


}
