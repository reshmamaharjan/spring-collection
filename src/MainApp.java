import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @Author Reshma
 * @Date 6/26/2018
 * @Month 06
 **/
public class MainApp {
    public static void main(String[] args) {
        ApplicationContext ac=new ClassPathXmlApplicationContext("applicationContext.xml");
        JavaCollection obj=(JavaCollection)ac.getBean("collection");
        System.out.println("List elements:"+obj.getAddList());
        System.out.println("\nSet elements:"+obj.getAddSet());
        System.out.println("\nMap elements:"+obj.getAddMAp());
        System.out.println("\nProperties elements:"+obj.getAddProp());
    }
}
